package com.example.mob201_ps10222_lab01;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class PS10222_LAB1 extends AppCompatActivity {

    Button btnKhoiTao, btnDung, btnTimKiem, btnTraLaiXuat;
    EditText edTimKiem, edKiTu;
    Spinner spnDSNganHang;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ps10222__lab1__cau1_2_3);
        // Ánh xạ:
        btnKhoiTao = findViewById(R.id.btnKhoiTao);
        btnDung = findViewById(R.id.btnDung);
        btnTimKiem = findViewById(R.id.btnTimKiem);
        edTimKiem = findViewById(R.id.edTimKiem);
        edKiTu = findViewById(R.id.edChonKiTu);
        spnDSNganHang = findViewById(R.id.spnChonNganHang);
        btnTraLaiXuat = findViewById(R.id.btnTraLaiXuat);

        List<String> list = new ArrayList<>();
        list.add("Sacombank");
        list.add("Vietcombank");
        list.add("VPBank");
        list.add("MBank");

        ArrayAdapter<String> adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item,list);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_item);
        spnDSNganHang.setAdapter(adapter);
        spnDSNganHang.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(PS10222_LAB1.this, spnDSNganHang.getSelectedItem().toString(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        btnKhoiTao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(PS10222_LAB1.this, LT14307Service.class);
                // -- Câu 2 --
                // Khởi tạo Bundle:
                Bundle bundle = new Bundle();
                // Khai báo các bộ giá trị và gán vào Bundle:
                bundle.putInt("StuId",01);
                bundle.putString("StuName", "Kiên");
                bundle.putString("Class", "LT14307");
                // Gắn Bundle vào Intent:
                i.putExtra("student",bundle);
                //  -----------
                // Khởi tạo Service
                startService(i);
            }
        });

        btnDung.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(PS10222_LAB1.this, LT14307Service.class);
                stopService(i);
            }
        });

        btnTimKiem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validate() && validateChuoi()){
                    Intent intent = new Intent(PS10222_LAB1.this, LT14307Service_cau3.class);
                    int dem = 0;
                    String check = edKiTu.getText().toString().trim();
                    for (int i = 0; i < edTimKiem.getText().length(); i++) {
                        String kiTu = String.valueOf(edTimKiem.getText().charAt(i));
                        if (kiTu.equalsIgnoreCase(check)) {
                            dem++;
                        }
                    }
                    // Khởi tạo Bundle:
                    Bundle bundle01 = new Bundle();
                    // Khai báo các bộ giá trị và gán vào Bundle:
                    bundle01.putInt("KiTu",dem);
                    bundle01.putString("check",check);
                    // Gắn Bundle vào Intent:
                    intent.putExtra("ketqua",bundle01);
                    // Khởi tạo Service
                    startService(intent);
                }
            }
        });

        // CÂU 4: ----------------------

        btnTraLaiXuat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String ten = spnDSNganHang.getSelectedItem().toString().trim();
                String laixuat = null;
                Intent intent = new Intent(PS10222_LAB1.this, LT14307Service_cau4.class);
                if(ten == "Sacombank"){
                    laixuat = "2%";
                } else if (ten == "Vietcombank"){
                    laixuat = "1%";
                } else if (ten == "VPBank"){
                    laixuat = "1.8%";
                } else if (ten == "MBBank"){
                    laixuat = "1.5%";
        }
                // Khởi tạo Bundle:
                Bundle bundle02 = new Bundle();
                // Khai báo các bộ giá trị và gán vào Bundle:
                bundle02.putString("LaiXuat",laixuat);
                // Gắn Bundle vào Intent:
                intent.putExtra("tinhlx",bundle02);
                // Khởi tạo Service
                startService(intent);
            }
        });
    }

    private boolean validate() {
        String kiTu = edKiTu.getText().toString().trim();
        if (kiTu.isEmpty()) {
            edKiTu.setError("Mời nhập kí tự");
            return false;
        } else if (kiTu.length() > 1) {
            edKiTu.setError("Bạn chỉ được nhập tối đa 1 kí tự");
            return false;
        } else {
            edKiTu.setError(null);
            return true;
        }
    }
    private boolean validateChuoi() {
        String chuoiKiTu = edTimKiem.getText().toString().trim();
        if (chuoiKiTu.isEmpty()) {
            edTimKiem.setError("Mời nhập chuỗi");
            return false;
        } else {
            edTimKiem.setError(null);
            return true;
        }
    }
}
