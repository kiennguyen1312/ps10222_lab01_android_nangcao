package com.example.mob201_ps10222_lab01;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.widget.Toast;

public class LT14307Service_cau3 extends Service {
    public LT14307Service_cau3() {
    }
    @Override
    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
        // Lấy Bundle từ Intent:
        Bundle bundle01 = intent.getBundleExtra("ketqua");
        // Lấy giá trị từ Bundle:
        int KiTu = bundle01.getInt("KiTu");
        String check = bundle01.getString("check");
        // Hiển thị kết quả lên màn hình:
        String xuat = "Số lần xuất hiện của kí tự "+check+" là:" + KiTu;
        Toast.makeText(this, xuat, Toast.LENGTH_SHORT).show();
    }



    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}