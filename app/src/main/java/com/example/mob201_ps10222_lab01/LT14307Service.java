package com.example.mob201_ps10222_lab01;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.widget.Toast;

public class LT14307Service extends Service {

    MediaPlayer player;

    @Override
    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
        // Lấy Bundle từ Intent:
        Bundle bundle = intent.getBundleExtra("student");
        // Lấy giá trị từ Bundle:
        int StuId = bundle.getInt("StuId");
        String StuName = bundle.getString("StuName");
        String Class = bundle.getString("Class");
        // Hiển thị kết quả lên màn hình:
        String content = "Thêm sinh viên thành công!\nThông tin sinh viên\nSinh viên:"+StuId+" "+StuName;
        content += "\nLớp:" + Class;
        Toast.makeText(this, content, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onCreate() {
        super.onCreate();
//        Toast.makeText(this, "onCreate", Toast.LENGTH_SHORT).show();
        player = MediaPlayer.create(this,R.raw.nhac);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
//        Toast.makeText(this, "onStartCommand", Toast.LENGTH_SHORT).show();
        player.start();
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        Toast.makeText(this, "onDestroy", Toast.LENGTH_SHORT).show();
        player.stop();
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
